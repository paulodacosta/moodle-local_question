FUNCTIONS

Nome: local_question_get_categories
Param: null/vazio
/local/question/client/client.php?functionname=local_question_get_categories

Nome: local_question_get_questions_by_category
Param: ID da categoria
/local/question/client/client.php?functionname=local_question_get_questions_by_category&param=4

Nome: local_question_get_questions_by_ids
Param: Array IDs de questões
/local/question/client/client.php?functionname=local_question_get_questions_by_ids&param[]=12&param[]=26

Nome: local_question_get_question_random_from_categories
Param: Array IDs de categorias
/local/question/client/client.php?functionname=local_question_get_question_random_from_categories&param[]=12&param[]=4

Nome: local_question_get_question
Param: ID da questão
/local/question/client/client.php?functionname=local_question_get_question&param=26